# Commentary Deployment

Deployment setup for a docker-compose stack of the Hannah Arendt Commentary Web Application.

## Configuration

Name | Default | Description
-----|---------|------------
`cd_ingress_port` | 8080 | The port the nginx ingress service exposes to the host
